
$(function(){
	var $spBtn = $('.sp-menu-btn'),
		$spNav = $('.sp-nav');

	var $slideImages = $('.slide-images'),
		$slideItem = $slideImages.find('li'),
		$sliderBtn = $('.slider-btn'),
		$slideBtnPrev = $sliderBtn.find('.prev-btn'),
		$slideBtnNext = $sliderBtn.find('.next-btn'),
		$slideNumber = $sliderBtn.find('.slide-number'),
		$bubbleCircle = $('#toppage .bubble-circle'),
		$bubbleCircleLarge = $('<div class="bubble-circle-large"></div>'),
		$bubbleCircleSmall = $('<div class="bubble-circle-small"></div>'),
		$aboutSectionImg = $('.about-wrapper .section-images'),
		$anotherSectionImg = $('.another-wrapper .section-images'),
		$navItems = $('.nav-list .nav-item a'),
		$scrollTopBtn = $('.scrollTopBtn'),
		slideItemLen = $slideItem.length,
		currentIndex = 0;

	$navItems.on('click', function(){
		var navTarget = $($(this).attr('href')).offset().top;
		$('html, body').animate({
			scrollTop: navTarget
		}, 500);

		return false;
	});

	$(window).on('scroll', function(){
		var scrollTop = $(this).scrollTop(),
			windowHeighthalf = window.innerHeight / 2,
			documentHeight = $(document).height();
		if(scrollTop > windowHeighthalf){
			$scrollTopBtn.fadeIn(100);
		} else {
			$scrollTopBtn.fadeOut(100);
		}
	});

	$scrollTopBtn.on('click', function(){
		$('html, body').animate({
			scrollTop: 0
		}, 300);
	});

	var bubbleColorArray = [
		'bubble-color-orenge',
		'bubble-color-grape',
		'bubble-color-apple',
		'bubble-color-peach',
		'bubble-color-lemon'
	];
	var bubbleColorArrayLen = bubbleColorArray.length;

	$(window).scroll(function(){
		var wScroll = $(this).scrollTop(),
			screenH = $(this).innerHeight();

		if(wScroll > $aboutSectionImg.offset().top - (screenH / 2)){
			$aboutSectionImg.addClass('is-fadeUp');
		}
		if(wScroll > $anotherSectionImg.offset().top - (screenH / 2)){
			$anotherSectionImg.addClass('is-fadeUp');
		}
	});

	function bubbleColor(index){
		$bubbleCircle.find('div:not(index)').fadeOut(1000).queue(function(){this.remove()});
		if(index <= bubbleColorArrayLen){
			$bubbleCircleLarge.clone().hide().appendTo($bubbleCircle).addClass(bubbleColorArray[index]).fadeIn(1000);
			$bubbleCircleSmall.clone().hide().appendTo($bubbleCircle).addClass(bubbleColorArray[index]).fadeIn(1000);
		}
	}

	function slideShow(index){
		var $current = $slideItem.filter(':visible'),
			$target = $slideItem.eq(index),
			viewNumber = index + 1;
		$slideNumber.text(viewNumber + '/5');
		$current.removeClass('is-fadeIn');
		$target.addClass('is-fadeIn');
	}

	$slideBtnPrev.on('click', function(){
		currentIndex--;
		if(currentIndex < 0){
			currentIndex = slideItemLen - 1;
		}
		slideShow(currentIndex);
		bubbleColor(currentIndex);
	});
	$slideBtnNext.on('click', function(){
		currentIndex++;
		if(currentIndex >= slideItemLen){
			currentIndex = 0;
		}
		slideShow(currentIndex);
		bubbleColor(currentIndex);
	});

	$spBtn.on('click', function(){
		$spNav.fadeIn();
	});
	$spNav.on('click', function(){
		$spNav.fadeOut();
	});

	slideShow(currentIndex);
	bubbleColor(currentIndex);
});